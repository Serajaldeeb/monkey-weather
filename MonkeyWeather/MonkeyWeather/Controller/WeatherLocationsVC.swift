//
//  WeatherLocationsVC.swift
//  MonkeyWeather
//
//  Created by User on 5/8/19.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class WeatherLocationsVC: UIViewController {
    @IBOutlet weak var currentDayLB: UILabel!
    @IBOutlet weak var countryNameLb: UILabel!
    @IBOutlet weak var weatherStatusLB: UILabel!
    @IBOutlet weak var currentTempLB: UILabel!
    @IBOutlet weak var humadityLB: UILabel!
    @IBOutlet weak var maxTempLB: UILabel!
    @IBOutlet weak var minTempLB: UILabel!
    @IBOutlet weak var loactionWeatherTableView: UITableView!
    
    var longitude: Double!
    var latitude: Double!
    var currentDayRef: String!
    var indexDay: Int!
    var currentLocationWeather: Climte!
    var fiveDayLocationWeather = [FiveDayWeatherHourly]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loactionWeatherTableView.delegate = self
        loactionWeatherTableView.dataSource = self
        getCurrntLocationData()
        getFiveDayLocationData()
        
        
    }
    
    func getCurrntLocationData() {
        GetsNetworking.getCurrentWeatherLocation(lat: latitude, lon: longitude, Success: { myObj in
            self.currentLocationWeather = myObj
            print(self.currentLocationWeather)
            DispatchQueue.main.async {
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEEE"
                let currentTDay = dateFormatter.string(from: date)
                self.currentDayRef = currentTDay
                self.countryNameLb.text = self.currentLocationWeather.sys.country + " " + self.currentLocationWeather.name
                self.weatherStatusLB.text = self.currentLocationWeather.weather[0].main
                self.currentTempLB.text = "\(self.currentLocationWeather.main.temp)°"
                self.humadityLB.text = "\(self.currentLocationWeather.main.humidity)%"
                self.minTempLB.text = "\(self.currentLocationWeather.main.tempMin)°"
                self.maxTempLB.text = "\(self.currentLocationWeather.main.tempMax)°"
                self.currentDayLB.text = currentTDay
            }
            
        }) { (error) in
            print(error)
        }
    }
    
    func getFiveDayLocationData() {
        GetsNetworking.getFiveDayWeatherLocation(lat: latitude, lon: longitude, Success: { myObj in
             self.fiveDayLocationWeather = [myObj]
            DispatchQueue.main.async {
                self.loactionWeatherTableView.reloadData()
            }
            
        }) { (error) in
            print(error)
        }
        
    }
    
}
extension WeatherLocationsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = loactionWeatherTableView.dequeueReusableCell(withIdentifier: "fiveDayLocationWeather", for: indexPath) as! WeatherLocationCell
        indexDay = indexPath.row
        if !fiveDayLocationWeather.isEmpty {
            cell.setLocationWeather(indexOfDay: self.indexDay,currentDay: self.currentDayRef , fiveDayWeather: self.fiveDayLocationWeather)        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
