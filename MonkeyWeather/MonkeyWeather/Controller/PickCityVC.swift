//
// PickCityVC.swift
//  Monkey Weather
//
//  Created by User on 4/17/19.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var city: String!
    let current = CurrentWeatherVC()
    //MARK:- View Life Cycle
    @IBOutlet weak var cityNameTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func getWeatherByCityName(_ sender: Any) {
        self.performSegue(withIdentifier: "cityWeather", sender: self)
    }
    //MARK:- Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "cityWeather"){
            let currentCity = segue.destination as! CurrentWeatherVC
            currentCity.cityNamess = cityNameTxt.text
        }
        
    }
    
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
