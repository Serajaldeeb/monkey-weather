//
//  CurrentWeatherVC.swift
//  Monkey Weather
//
//  Created by User on 4/22/19.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class CurrentWeatherVC: UIViewController {
    @IBOutlet weak var weatherTableView: UITableView!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    @IBOutlet weak var currentDay: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var countryCity: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var weatherData: CurrentWeather!
    var weeklyWeather: CurrentWeather!
    var cityNamess: String!
    var indexOfDay: Int!
    var currentDayRef: String!
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getDataWeatherCurrentDay()
        getDataWeatherFiveDay {
            DispatchQueue.main.sync {
                self.weatherTableView.reloadData()
            }
        }
    }
    
    //MARK:- Set Background Image
    func setBackground(statsu: String){
        switch weatherData.current.condition.text {
        case "Cloudy":
            backgroundImage.image = UIImage(named: ""); break
        case "Sunny":
            backgroundImage.image = UIImage(named: "Sunny"); break
        case "Rainy":
            backgroundImage.image = UIImage(named: "Rainy"); break
        case "Clear":
            backgroundImage.image = UIImage(named: "Sunny"); break
        default:
            backgroundImage.image = UIImage(named: "Sunny")
        }
        
    }
    
    func getDataWeatherCurrentDay()  {
        GetsNetworking.getWeather(cityName: cityNamess, { apiObj in
            self.weatherData = apiObj
            DispatchQueue.main.async {
                let date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "EEEE"
                let currentTDay = dateFormatter.string(from: date)
                self.currentDayRef = currentTDay
                
                self.setBackground(statsu: self.weatherData.current.condition.text)
                self.countryCity.text = self.weatherData.location.country + " " + self.weatherData.location.name
                self.temp.text = "\(self.weatherData.current.tempC)°"
                self.status.text = (self.weatherData.current.condition.text)
                self.minTemp.text = "\(self.weatherData.forecast.forecastday[0].day.maxtempC)°"
                self.maxTemp.text = "\(self.weatherData.forecast.forecastday[0].day.mintempC)°"
                self.humidity.text = "\(self.weatherData.current.humidity)%"
                self.currentDay.text = currentTDay
            }
            
        }) {error in
            print(error)
        }
    }
    
    func getDataWeatherFiveDay(Success: @escaping () -> Void) {
        GetsNetworking.getSixteenWeatherday(cityName: cityNamess ,countryCode: "JO" , { apiObj in
            self.weeklyWeather = apiObj
            Success()
        }) { error in
            print(error)
        }
    }
}

extension CurrentWeatherVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cellLocation = weatherTableView.dequeueReusableCell(withIdentifier: "DropLocationCell", for: indexPath) as! DropLocationCell
            return cellLocation
        }
        else {
            let cellWeather = weatherTableView.dequeueReusableCell(withIdentifier: "weatherFiveDayCell", for: indexPath) as! DailyWeatherCell
            self.indexOfDay = indexPath.row
            
            if weeklyWeather != nil{
                cellWeather.setWeather( indexOfDay: self.indexOfDay, fiveDayWeather: self.weeklyWeather)
            }
            return cellWeather
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

