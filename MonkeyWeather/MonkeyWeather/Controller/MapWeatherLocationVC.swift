//
//  WeatherLocationVC.swift
//  MonkeyWeather
//
//  Created by User on 5/7/19.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit
import MapKit
class MapWeatherLocationVC: UIViewController {
    @IBOutlet weak var weatherLocationMap: MKMapView!
    let locationManager = CLLocationManager()
    var lon: Double!
    var lat: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        let mapPress = UITapGestureRecognizer(target: self, action: #selector(MapWeatherLocationVC.addAnnotation(_:)))
        // mapPress.minimumPressDuration = 1.0
        weatherLocationMap.addGestureRecognizer(mapPress)
        let span = MKCoordinateSpan(latitudeDelta: 0.600, longitudeDelta: 0.600)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 31.95340500994972, longitude: 36.04540288994844), span: span)
        self.weatherLocationMap.setRegion(region, animated: true)
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "DropInLocation"){
            let location = segue.destination as! WeatherLocationsVC
            location.latitude = lat
            location.longitude = lon
            print(lat)
            print(lon)
        }
        
    }
    
    
}

extension MapWeatherLocationVC: MKMapViewDelegate, CLLocationManagerDelegate {
    
    @objc func addAnnotation(_ recognizer: UIGestureRecognizer){
        let annotations = self.weatherLocationMap.annotations
        self.weatherLocationMap.removeAnnotations(annotations)
        let touchedAt = recognizer.location(in: self.weatherLocationMap)
        let newCoordinates : CLLocationCoordinate2D = weatherLocationMap.convert(touchedAt, toCoordinateFrom: self.weatherLocationMap)
        lat = newCoordinates.latitude
        lon = newCoordinates.longitude
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = newCoordinates
        self.weatherLocationMap.addAnnotation(annotation)
        self.performSegue(withIdentifier: "DropInLocation", sender: self)
    }
    
    
}
