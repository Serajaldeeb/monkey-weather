//
//  GetRequest.swift
//  Monkey Weather
//
//  Created by User on 4/17/19.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation

struct GetsNetworking {
    
    static func getWeather(cityName: String,_ success: @escaping (_ myObj: CurrentWeather) -> Void ,_ failure: @escaping (Error) -> Void){
        // let currentWeatherAPI = "\(BASE_URL_CURR)\(CITY_NAME)\(cityName)\(APP_ID)\(API_KEY)"
        let currentWeatherAPI = "https://api.apixu.com/v1/forecast.json?key=de4c85c1810b4a1082881602191205&q="+"\(cityName)"
        print(currentWeatherAPI)
        let currentApiURL = URL(string: currentWeatherAPI)!
        _ = URLSession.shared.dataTask(with: currentApiURL){  (data,response,error) in
            if error != nil{
                failure(error!)
            }
            let myClimte = try! JSONDecoder().decode(CurrentWeather.self, from: data!)
            success(myClimte)
            }.resume()
    }
    
    static func getSixteenWeatherday(cityName: String ,countryCode: String ,_ success: @escaping (_ myObj: CurrentWeather)->Void ,_ faliure: @escaping (Error)->Void ){
        let FiveDayWeatherHourlyAPI = "https://api.apixu.com/v1/forecast.json?key=de4c85c1810b4a1082881602191205&q="+"\(cityName)&days=9"
        let fiveDayApi = URL(string: FiveDayWeatherHourlyAPI)!
        _ = URLSession.shared.dataTask(with: fiveDayApi){ (data, response, error) in
            if error != nil {
                faliure(error!)
            }
            let myFiveDayWeather = try! JSONDecoder().decode(CurrentWeather.self, from: data!)
            success(myFiveDayWeather)
            }.resume()
        
    }
    static func getCurrentWeatherLocation(lat: Double, lon: Double, Success: @escaping (_ myObj: Climte) -> Void, Failuer: @escaping (Error) -> Void ) {
        let currentLocationWeather = "\(BASE_URL_CURR)\(LAT)\(lat)&\(LON)\(lon)\(APP_ID)\(API_KEY)"
        let currentLocationApiCall = URL(string: currentLocationWeather)!
        print(currentLocationWeather)
        _ = URLSession.shared.dataTask(with: currentLocationApiCall) { (data, response, error) in
            if error != nil {
                Failuer(error!)
            }
            let myCurrentLocationWeather = try! JSONDecoder().decode(Climte.self, from: data!)
            Success(myCurrentLocationWeather)
            
            }.resume()
        
    }
    
    static func getFiveDayWeatherLocation(lat: Double, lon: Double, Success: @escaping (_ myObj: FiveDayWeatherHourly) -> Void, Failuer: @escaping (Error) -> Void ) {
        let fiveDayLocationWeather = "\(BASE_URL_FIVE_DAY)\(LAT)\(lat)&\(LON)\(lon)\(APP_ID)\(API_KEY)"
        print(fiveDayLocationWeather)
        let fiveDayLocationApiCall = URL(string: fiveDayLocationWeather)!
        _ = URLSession.shared.dataTask(with: fiveDayLocationApiCall) { (data, response, error) in
            if error != nil {
                Failuer(error!)
            }
            let myFiveWeatherLocation = try! JSONDecoder().decode(FiveDayWeatherHourly.self, from: data!)
            Success(myFiveWeatherLocation)
            
            }.resume()
    }
    
    static func getImageIcon(urlData: String, Success: @escaping (Data) -> Void, Failuer: @escaping (Error) -> Void  ) {
        let urlData1 = "https://api.apixu.com" + urlData
        let imageURL = URL(string: urlData1)
        print(urlData1)
        _ = URLSession.shared.dataTask(with: imageURL!) { (data, response, error) in
            if error != nil {
                Failuer(error!)
            }
            Success(data!)
            
            }.resume()
        
        
    }
    
    
    
    
}
