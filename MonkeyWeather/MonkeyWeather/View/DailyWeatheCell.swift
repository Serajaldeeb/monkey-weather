//
//  DailyWeatheCell.swift
//  Monkey Weather
//
//  Created by User on 4/24/19.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class DailyWeatherCell: UITableViewCell {
    @IBOutlet weak var dayInWeek: UILabel!
    @IBOutlet weak var weatherStatusImage: UIImageView!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var minTemp: UILabel!
    
    func setWeather(indexOfDay: Int, fiveDayWeather: CurrentWeather){
      //  let url = URL(string:"https://api.apixu.com" + fiveDayWeather.forecast.forecastday[indexOfDay].day.condition.icon)!
         let url = URL(string: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg")!
        downloadImage(from: url)
        
        maxTemp.text = "\(fiveDayWeather.forecast.forecastday[indexOfDay].day.maxtempC)°"
        minTemp.text = "\(fiveDayWeather.forecast.forecastday[indexOfDay].day.mintempC)°"
        dayInWeek.text = fiveDayWeather.forecast.forecastday[indexOfDay].date
        
        //        switch fiveDayWeather.forecast.forecastday[indexOfDay-1].day.condition.text {
        //        case "Clouds":
        //            weatherStatusImage.image = UIImage(named: "cloudyDay");break
        //        case "Clear":
        //            weatherStatusImage.image = UIImage(named: "Sunnyday");break
        //        case "Rain":
        //            weatherStatusImage.image = UIImage(named: "RainyDay");break
        //        default:
        //            weatherStatusImage.image = UIImage(named: "Sunnyday")
        
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                self.weatherStatusImage.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
