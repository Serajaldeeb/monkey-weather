//
//  WeatherLocationCell.swift
//  MonkeyWeather
//
//  Created by User on 5/7/19.
//  Copyright © 2019 User. All rights reserved.
//

import UIKit

class WeatherLocationCell: UITableViewCell {
    
    @IBOutlet weak var weatherStatusImage: UIImageView!
    @IBOutlet weak var weekDayLB: UILabel!
    
    @IBOutlet weak var minTempLB: UILabel!
    @IBOutlet weak var maxTempLB: UILabel!
    
    func setLocationWeather(indexOfDay: Int, currentDay: String, fiveDayWeather: [FiveDayWeatherHourly])  {
        if !fiveDayWeather.isEmpty {
            
            maxTempLB.text = "\(Int(round(fiveDayWeather[0].list[indexOfDay].main.tempMax/12.4)))°"
            minTempLB.text = "\(Int(round(fiveDayWeather[0].list[indexOfDay].main.tempMin/12.4)))°"
            
            switch fiveDayWeather[0].list[indexOfDay].weather[0].main {
            case "Clouds":
                weatherStatusImage.image = UIImage(named: "cloudyDay");break
            case "Clear":
                weatherStatusImage.image = UIImage(named: "Sunnyday");break
            case "Rain":
                weatherStatusImage.image = UIImage(named: "RainyDay");break
            default:
                weatherStatusImage.image = UIImage(named: "Sunnyday")
            }
            weekDayLB.text = currentDay
        }
    }
}

