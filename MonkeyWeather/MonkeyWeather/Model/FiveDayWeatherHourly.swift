//
//  SixteenWeatherClimte.swift
//  Monkey Weather
//
//  Created by User on 4/24/19.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation

struct FiveDayWeatherHourly: Codable {
    let cod: String
    let message: Double?
    let cnt: Int
    let list: [List]
    let city: City
}
enum MainEnum: String, Codable {
    case clear = "Clear"
    case clouds = "Clouds"
    case rain = "Rain"
}




