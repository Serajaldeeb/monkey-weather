//
//  WindModel.swift
//  Monkey Weather
//
//  Created by User on 4/29/19.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation

struct Wind: Codable {
    let speed: Double
    let deg: Double?
}
