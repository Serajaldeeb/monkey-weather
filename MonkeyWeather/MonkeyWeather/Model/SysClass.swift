//
//  SysClass.swift
//  Monkey Weather
//
//  Created by User on 4/29/19.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation

struct SysClass: Codable {
    let pod: Pod
   
    enum Pod: String, Codable {
        case d = "d"
        case n = "n"
    }
}
