//
//  Coord.swift
//  Monkey Weather
//
//  Created by User on 4/29/19.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation

struct Coord: Codable {
    let lon, lat: Double
}
