//
//  RainModel.swift
//  Monkey Weather
//
//  Created by User on 4/29/19.
//  Copyright © 2019 User. All rights reserved.
//

import Foundation

struct Rain: Codable {
    let the3H: Double?
    
    enum CodingKeys: String, CodingKey {
        case the3H = "3h"
    }
}
